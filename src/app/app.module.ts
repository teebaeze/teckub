import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CounterComponent } from './components/counter.component';
import { MediaComponent } from './components/media.component';
import { HomeComponent } from './controllers/home.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CounterComponent,MediaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
