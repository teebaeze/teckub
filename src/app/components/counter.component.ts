import {Component, Input, Output,EventEmitter} from "@angular/core"

@Component({
    selector:'tee-counter',
    templateUrl:'./counter.component.html',

})
export class CounterComponent {
    @Input() count:number =0;
    @Output() countChange: EventEmitter<number> = new EventEmitter<number>();
    @Output() addNewItem: EventEmitter<string> =new EventEmitter() 
    increment(){
        this.count ++;
        this.countChange.emit(this.count)

    }
    decrement(){ 
        this.count--;
      
        if(this.count<1){
            this.count = 0
        }
    
    }
    addItem(value:string){
        this.addNewItem.emit(value)

    }
}