import {Component,Output,EventEmitter} from "@angular/core"


@Component({
    selector:'tee-media',
    templateUrl:'./media.component.html',
   // styleUrls:['./']
})

export class MediaComponent{
    @Output() addnewevent:EventEmitter<string>=new EventEmitter;

addItem(item:string){
    this.addnewevent.emit(item)

}

} 